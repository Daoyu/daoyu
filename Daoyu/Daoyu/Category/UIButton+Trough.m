//
//  UIButton+Trough.m
//  DrivTrain
//
//  Created by Wisdom on 2017/6/7.
//  Copyright © 2017年 Wisdom. All rights reserved.
//

#import "UIButton+Trough.h"
#import <objc/runtime.h>

static const void *tagKey = &tagKey;
static const void *indexKey = @"indexKey";
@implementation UIButton (Trough)

- (NSString *)userId
{
    return objc_getAssociatedObject(self, tagKey);
}

- (void)setUserId:(NSString *)userId
{
    objc_setAssociatedObject(self, tagKey, userId, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSIndexPath *)indexPath
{
    return objc_getAssociatedObject(self, indexKey);
}

- (void)setIndexPath:(NSIndexPath *)indexPath
{
    objc_setAssociatedObject(self, indexKey, indexPath, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
