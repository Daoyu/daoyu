//
//  UIViewController+LeftItem.h
//  DrivTrain
//
//  Created by Wisdom on 2017/5/5.
//  Copyright © 2017年 Wisdom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (LeftItem)

- (void)setBackItem;
- (void)setCloseItem;

@end
