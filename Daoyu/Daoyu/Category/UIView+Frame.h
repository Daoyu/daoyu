//
//  UIView+Frame.h
//  UIView+Frame
//
//  Created by ios-022 on 2017/6/5.
//  Copyright © 2017年 ios-022. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)



@property(nonatomic,assign) CGFloat width;
@property(nonatomic,assign) CGFloat height;
@property(nonatomic,assign) CGFloat x;
@property(nonatomic,assign) CGFloat y;

@property(nonatomic,assign) CGFloat centerX;
@property(nonatomic,assign) CGFloat centerY;

@property(nonatomic,assign) CGFloat right;



@property (assign, nonatomic) CGFloat    top;
@property (assign, nonatomic) CGFloat    bottom;
@property (assign, nonatomic) CGFloat    left;







@end
