//
//  UIButton+Trough.h
//  DrivTrain
//
//  Created by Wisdom on 2017/6/7.
//  Copyright © 2017年 Wisdom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Trough)

@property (nonatomic,copy)NSString *userId;

@property (nonatomic,strong) NSIndexPath *indexPath;

@end
