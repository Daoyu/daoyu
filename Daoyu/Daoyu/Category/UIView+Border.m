//
//  UIView+Border.m
//  Dodsport_Business
//
//  Created by Dodsport on 2017/9/11.
//  Copyright © 2017年 edwardLxh. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)

-(void)addTopBorderWithColor:(UIColor*)color andWidth:(CGFloat)borderWidth
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, 0, self.frame.size.width, borderWidth);
    [self.layer addSublayer:border];
}

-(void)addLeftBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, 0, borderWidth, self.frame.size.height);
    [self.layer addSublayer:border];
}
-(void)addRightBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(self.frame.size.width - borderWidth, 0, borderWidth, self.frame.size.height);
    [self.layer addSublayer:border];
}
-(void)addBottomBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, borderWidth);
    [self.layer addSublayer:border];
}

- (void)addBottomBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth andViewLength:(CGFloat)viewLength
{
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, viewLength, borderWidth);
    [self.layer addSublayer:border];
}

@end
