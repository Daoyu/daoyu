//
//  UIView+Border.h
//  Dodsport_Business
//
//  Created by Dodsport on 2017/9/11.
//  Copyright © 2017年 edwardLxh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Border)

-(void)addBottomBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;
-(void)addLeftBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;
-(void)addRightBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;
-(void)addTopBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;
- (void)addBottomBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth andViewLength:(CGFloat)viewLength;
@end
