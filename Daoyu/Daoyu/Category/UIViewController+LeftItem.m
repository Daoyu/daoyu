//
//  UIViewController+LeftItem.m
//  DrivTrain
//
//  Created by Wisdom on 2017/5/5.
//  Copyright © 2017年 Wisdom. All rights reserved.
//

#import "UIViewController+LeftItem.h"

@implementation UIViewController (LeftItem)
- (void)setBackItem
{
    UIButton *imgV = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgV setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [imgV addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:imgV];

    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setCloseItem
{
    UIButton *imgV = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgV setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [imgV addTarget:self action:@selector(closeAll) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:imgV];
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeAll
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
