//
//  AppDelegate.h
//  Daoyu
//
//  Created by Wp on 2019/7/18.
//  Copyright © 2019 Reo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

