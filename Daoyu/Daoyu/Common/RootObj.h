//
//  RootObj.h
//  Daoyu
//
//  Created by fong on 2019/7/19.
//  Copyright © 2019 Reo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RootObj : NSObject

@property (nonatomic,strong) UITabBarController *tabbar;

+ (instancetype)shared;


@end

NS_ASSUME_NONNULL_END
