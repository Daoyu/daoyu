//
//  RootObj.m
//  Daoyu
//
//  Created by fong on 2019/7/19.
//  Copyright © 2019 Reo. All rights reserved.
//

#import "RootObj.h"

static RootObj *_sharedSingleton = nil;
@implementation RootObj

+ (instancetype)shared {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //不能再使用alloc方法
        //因为已经重写了allocWithZone方法，所以这里要调用父类的分配空间的方法
        _sharedSingleton = [[super allocWithZone:NULL] init];
    });
    return _sharedSingleton;
}

- (UITabBarController *)tabbar
{
    if (!_tabbar) {
        _tabbar = [[UITabBarController alloc] init];
        
        UIViewController *vc = [[NSClassFromString(@"MessageVC") alloc] init];
        UINavigationController *nav1 = [[CozNavigationController alloc] initWithRootViewController:vc];
        nav1.tabBarItem.title = @"聊天";
        UIViewController *vc1 = [[NSClassFromString(@"RedPVC") alloc] init];
        vc1.title = @"红包";
        UINavigationController *nav2 = [[CozNavigationController alloc] initWithRootViewController:vc1];
        UIViewController *vc2 = [[NSClassFromString(@"MineVC") alloc] init];
        vc2.title = @"我";
        UINavigationController *nav3 = [[CozNavigationController alloc] initWithRootViewController:vc2];
        
        
        _tabbar.viewControllers = @[nav1,nav2,nav3];
        
    }
    return _tabbar;
}

@end
